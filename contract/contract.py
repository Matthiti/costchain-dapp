# Fungible Assets - FA12
# Inspired by https://gitlab.com/tzip/tzip/blob/master/A/FA1.2.md

import smartpy as sp


# The metadata below is just an example, it serves as a base,
# the contents are used to build the metadata JSON that users
# can copy and upload to IPFS.
TZIP16_Metadata_Base = {
    "name"          : "JimsterToken",
    "description"   : "Used to track expenses",
    "authors"       : [
        "Matthijs Roelink <matthijs@roelink.eu>"
    ],
    "homepage"      : "https://roelink.eu",
    "interfaces"    : [
        "TZIP-007-2021-04-17",
        "TZIP-016-2021-04-17"
    ],
}

# A collection of error messages used in the contract.
class FA12_Error:
    def make(s): return ("FA1.2_" + s)

    NotAdmin                        = make("NotAdmin")
    InsufficientBalance             = make("InsufficientBalance")
    UnsafeAllowanceChange           = make("UnsafeAllowanceChange")
    Paused                          = make("Paused")
    NotAllowed                      = make("NotAllowed")
    NoSetBalanceRequest             = make("NoSetBalanceRequest")
    NotAllAdminsApproved            = make("NotAllAdminsApproved")


##
## ## Meta-Programming Configuration
##
## The `FA12_config` class holds the meta-programming configuration.
##
class FA12_config:
    def __init__(
        self,
        support_upgradable_metadata         = False,
        use_token_metadata_offchain_view    = True,
    ):
        self.support_upgradable_metadata = support_upgradable_metadata
        # Whether the contract metadata can be upgradable or not.
        # When True a new entrypoint `change_metadata` will be added.

        self.use_token_metadata_offchain_view = use_token_metadata_offchain_view
        # Include offchain view for accessing the token metadata (requires TZIP-016 contract metadata)

class FA12_common:
    def normalize_metadata(self, metadata):
        """
            Helper function to build metadata JSON (string => bytes).
        """
        for key in metadata:
            metadata[key] = sp.utils.bytes_of_string(metadata[key])

        return metadata

class FA12_core(sp.Contract, FA12_common):
    def __init__(self, config, **extra_storage):
        self.config = config

        self.init(
            balances = sp.map(tkey = sp.TAddress, tvalue = sp.TRecord(approvals = sp.TMap(sp.TAddress, sp.TNat), balance = sp.TNat)),
            totalSupply = 0,
            setBalanceRequests = sp.map(tkey = sp.TAddress, tvalue = sp.TRecord(newBalance = sp.TNat, approvals = sp.TSet(sp.TAddress))),
            **extra_storage
        )

    @sp.entry_point
    def transfer(self, params):
        sp.set_type(params, sp.TRecord(from_ = sp.TAddress, to_ = sp.TAddress, value = sp.TNat).layout(("from_ as from", ("to_ as to", "value"))))
        self._transfer(params.from_, params.to_, params.value)

    def _transfer(self, from_, to_, value):
        sp.verify(self.is_administrator(sp.sender) |
            (~self.is_paused() &
                ((from_ == sp.sender) |
                 (self.data.balances[from_].approvals[sp.sender] >= value))), FA12_Error.NotAllowed)
        self.addAddressIfNecessary(from_)
        self.addAddressIfNecessary(to_)
        sp.verify(self.data.balances[from_].balance >= value, FA12_Error.InsufficientBalance)
        self.data.balances[from_].balance = sp.as_nat(self.data.balances[from_].balance - value)
        self.data.balances[to_].balance += value
        sp.if (from_ != sp.sender) & (~self.is_administrator(sp.sender)):
            self.data.balances[from_].approvals[sp.sender] = sp.as_nat(self.data.balances[from_].approvals[sp.sender] - value)

    @sp.entry_point
    def approve(self, params):
        sp.set_type(params, sp.TRecord(spender = sp.TAddress, value = sp.TNat).layout(("spender", "value")))
        self.addAddressIfNecessary(sp.sender)
        sp.verify(~self.is_paused(), FA12_Error.Paused)
        alreadyApproved = self.data.balances[sp.sender].approvals.get(params.spender, 0)
        sp.verify((alreadyApproved == 0) | (params.value == 0), FA12_Error.UnsafeAllowanceChange)
        self.data.balances[sp.sender].approvals[params.spender] = params.value

    def addAddressIfNecessary(self, address):
        sp.if ~ self.data.balances.contains(address):
            self.data.balances[address] = sp.record(balance = 0, approvals = {})

    @sp.utils.view(sp.TNat)
    def getBalance(self, params):
        sp.if self.data.balances.contains(params):
            sp.result(self.data.balances[params].balance)
        sp.else:
            sp.result(sp.nat(0))

    @sp.utils.view(sp.TNat)
    def getAllowance(self, params):
        sp.if self.data.balances.contains(params.owner):
            sp.result(self.data.balances[params.owner].approvals.get(params.spender, 0))
        sp.else:
            sp.result(sp.nat(0))

    @sp.utils.view(sp.TNat)
    def getTotalSupply(self, params):
        sp.set_type(params, sp.TUnit)
        sp.result(self.data.totalSupply)

    # this is not part of the standard but can be supported through inheritance.
    def is_paused(self):
        return sp.bool(False)

    # this is not part of the standard but can be supported through inheritance.
    def is_administrator(self, sender):
        return sp.bool(False)

class FA12_mint_burn(FA12_core):
    def mint(self, address, value):
        self.addAddressIfNecessary(address)
        self.data.balances[address].balance += value
        self.data.totalSupply += value

    def burn(self, address, value):
        sp.verify(self.data.balances[address].balance >= value, FA12_Error.InsufficientBalance)
        self.data.balances[address].balance = sp.as_nat(self.data.balances[address].balance - value)
        self.data.totalSupply = sp.as_nat(self.data.totalSupply - value)

class FA12_extended_mint_burn(FA12_mint_burn):
    """
    When tokens are minted, the amount is added to all accounts.
    """
    @sp.entry_point
    def mint(self, params):
        sp.set_type(params, sp.TRecord(address = sp.TAddress, value = sp.TNat))
        sp.verify(self.is_administrator(sp.sender), FA12_Error.NotAdmin)
        sp.for address in self.data.balances.keys():
            super().mint(address, params.value)

    """
    When tokens are burned, the amount is removed from all accounts.
    """
    @sp.entry_point
    def burn(self, params):
        sp.set_type(params, sp.TRecord(address = sp.TAddress, value = sp.TNat))
        sp.verify(self.is_administrator(sp.sender), FA12_Error.NotAdmin)
        sp.for address in self.data.balances.keys():
            super().burn(address, params.value)

"""
Custom class to enable the 'transferWithMessage' function.
The `message` param is ignored, but stored in the transaction.
"""
class FA12_transfer_with_message(FA12_core):
    @sp.entry_point
    def transferWithMessage(self, params):
        sp.set_type(params, sp.TRecord(from_ = sp.TAddress, to_ = sp.TAddress, value = sp.TNat, message = sp.TString).layout(("from_ as from", ("to_ as to", ("value", "message")))))
        self._transfer(params.from_, params.to_, params.value)

"""
Custom class to enable setting the balance of an address to some value.
This required approval from all admins.
"""
class FA12_set_balance(FA12_core):
    @sp.entry_point
    def setBalance(self, params):
        sp.set_type(params, sp.TAddress)
        sp.verify(self.is_administrator(sp.sender), FA12_Error.NotAdmin)
        sp.verify(self.data.setBalanceRequests.contains(params), FA12_Error.NoSetBalanceRequest)
        sp.for admin in self.data.administrators.elements():
            sp.verify(self.data.setBalanceRequests[params].approvals.contains(admin), FA12_Error.NotAllAdminsApproved)
        self.addAddressIfNecessary(params)
        self.data.totalSupply = sp.as_nat(self.data.totalSupply - self.data.balances[params].balance) + self.data.setBalanceRequests[params].newBalance
        self.data.balances[params].balance = self.data.setBalanceRequests[params].newBalance
        del self.data.setBalanceRequests[params]

    @sp.entry_point
    def requestSetBalance(self, params):
        sp.set_type(params, sp.TRecord(address = sp.TAddress, newBalance = sp.TNat))
        sp.verify(self.is_administrator(sp.sender), FA12_Error.NotAdmin)
        self.data.setBalanceRequests[params.address] = sp.record(newBalance = params.newBalance, approvals = sp.set([sp.sender]))

    @sp.entry_point
    def approveSetBalance(self, params):
        sp.set_type(params, sp.TAddress)
        sp.verify(self.is_administrator(sp.sender), FA12_Error.NotAdmin)
        sp.verify(self.data.setBalanceRequests.contains(params), FA12_Error.NoSetBalanceRequest)
        self.data.setBalanceRequests[params].approvals.add(sp.sender)

    @sp.entry_point
    def denySetBalance(self, params):
        sp.set_type(params, sp.TAddress)
        sp.verify(self.is_administrator(sp.sender), FA12_Error.NotAdmin)
        sp.verify(self.data.setBalanceRequests.contains(params), FA12_Error.NoSetBalanceRequest)
        del self.data.setBalanceRequests[params]


class FA12_administrator(FA12_core):
    def is_administrator(self, sender):
        return self.data.administrators.contains(sender)

    # @sp.entry_point
    # def setAdministrators(self, params):
    #     sp.set_type(params, sp.TSet(sp.TAddress))
    #     sp.verify(self.is_administrator(sp.sender), FA12_Error.NotAdmin)
    #     self.data.administrators = params

    @sp.utils.view(sp.TSet(sp.TAddress))
    def getAdministrators(self, params):
        sp.set_type(params, sp.TUnit)
        sp.result(self.data.administrators)

class FA12_pause(FA12_core):
    def is_paused(self):
        return self.data.paused

    @sp.entry_point
    def setPause(self, params):
        sp.set_type(params, sp.TBool)
        sp.verify(self.is_administrator(sp.sender), FA12_Error.NotAdmin)
        self.data.paused = params

class FA12_token_metadata(FA12_core):
    """
        SPEC: https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md#token-metadata

        Token-specific metadata is stored/presented as a Michelson value of type (map string bytes).

        A few of the keys are reserved and predefined:

        >>    ""          : Should correspond to a TZIP-016 URI which points to a JSON representation of the token
                            metadata.

        >>    "name"      : Should be a UTF-8 string giving a “display name” to the token.

        >>    "symbol"    : Should be a UTF-8 string for the short identifier of the token (e.g. XTZ, EUR, …).

        >>    "decimals"  : Should be an integer (converted to a UTF-8 string in decimal) which defines the position of                   the decimal point in token balances for displaypurposes.
    """
    def set_token_metadata(self, metadata):
        """
            Store the token_metadata values in a big-map annotated %token_metadata
            of type (big_map nat (pair (nat %token_id) (map %token_info string bytes))).
        """
        self.update_initial_storage(
            token_metadata = sp.big_map(
                {
                    0: sp.record(token_id = 0, token_info = self.normalize_metadata(metadata))
                },
                tkey = sp.TNat,
                tvalue = sp.TRecord(token_id = sp.TNat, token_info = sp.TMap(sp.TString, sp.TBytes))
            )
        )

class FA12_contract_metadata(FA12_core):
    """
        SPEC: https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-16/tzip-16.md

        This class offers utilities to define and set TZIP-016 contract metadata.
    """
    def generate_tzip16_metadata(self):
        views = []

        def token_metadata(self, token_id):
            """
                This method will become an offchain view if the contract uses TZIP-016 metadata
                and the config `use_token_metadata_offchain_view` is set to TRUE.

                Return the token-metadata URI for the given token. (token_id must be 0)

                For a reference implementation, dynamic-views seem to be the
                most flexible choice.
            """
            sp.set_type(token_id, sp.TNat)
            sp.result(self.data.token_metadata[token_id])

        if self.usingTokenMetadata and self.config.use_token_metadata_offchain_view:
            self.token_metadata = sp.offchain_view(pure = True, doc = "Get Token Metadata")(token_metadata)
            views += [self.token_metadata]

        metadata = {
            **TZIP16_Metadata_Base,
            "views"         : views
        }

        self.init_metadata("metadata", metadata)

    def set_contract_metadata(self, metadata):
        """
           Set contract metadata
        """
        self.update_initial_storage(
            metadata = sp.big_map(self.normalize_metadata(metadata))
        )

        if self.config.support_upgradable_metadata:
            def update_metadata(self, key, value):
                """
                    An entry-point to allow the contract metadata to be updated.

                    Can be removed with `FA12_config(support_upgradable_metadata = False, ...)`
                """
                sp.verify(self.is_administrator(sp.sender), FA12_Error.NotAdmin)
                self.data.metadata[key] = value
            self.update_metadata = sp.entry_point(update_metadata)

class FA12(
    FA12_extended_mint_burn,
    FA12_set_balance,
    FA12_transfer_with_message,
    FA12_administrator,
    FA12_pause,
    FA12_token_metadata,
    FA12_contract_metadata,
    FA12_core
):
    def __init__(self, admins, config, token_metadata = None, contract_metadata = None):
        FA12_core.__init__(self, config, paused = False, administrators = admins)

        if token_metadata is None and contract_metadata is None:
            raise Exception(
            """\n
                Contract must contain at least of the following:
                    \t- TZIP-016 %metadata big-map,
                    \t- Token-specific-metadata through the %token_metadata big-map

                More info: https://gitlab.com/tzip/tzip/blob/master/proposals/tzip-7/tzip-7.md#token-metadata
            """
            )

        self.usingTokenMetadata = False
        if token_metadata is not None:
            self.usingTokenMetadata = True
            self.set_token_metadata(token_metadata)
        if contract_metadata is not None:
            self.set_contract_metadata(contract_metadata)

        # This is only an helper, it produces metadata in the output panel
        # that users can copy and upload to IPFS.
        self.generate_tzip16_metadata()

class Viewer(sp.Contract):
    def __init__(self, t):
        self.init(last = sp.none)
        self.init_type(sp.TRecord(last = sp.TOption(t)))
    @sp.entry_point
    def target(self, params):
        self.data.last = sp.some(params)

# Used to test offchain views
class TestOffchainView(sp.Contract):
    def __init__(self, f):
        self.f = f.f
        self.init(result = sp.none)

    @sp.entry_point
    def compute(self, data, params):
        b = sp.bind_block()
        with b:
            self.f(sp.record(data = data), params)
        self.data.result = sp.some(b.value)

if "templates" not in __name__:
    @sp.add_test(name = "FA12")
    def test():

        scenario = sp.test_scenario()
        scenario.h1("FA1.2 template - Fungible assets")

        scenario.table_of_contents()

        # sp.test_account generates ED25519 key-pairs deterministically:
        alice    = sp.test_account("Alice")
        bob      = sp.test_account("Bob")
        nonadmin = sp.test_account("Non admin")

        # Let's display the accounts:
        scenario.h1("Accounts")
        scenario.show([alice, bob])

        scenario.h1("Contract")
        token_metadata = {
            "decimals"    : "2",               # Mandatory by the spec
            "name"        : "JimsterToken",    # Recommended
            "symbol"      : "JIM",             # Recommended
            # Extra fields
            "icon"        : "https://costchain.roelink.eu/static/icon.png"
        }
        c1 = FA12(
            sp.set([alice.address, bob.address]),
            config              = FA12_config(support_upgradable_metadata = True),
            token_metadata      = token_metadata,
            contract_metadata   = None
        )
        scenario += c1

        scenario.h1("Offchain view - token_metadata")
        # Test token_metadata view
        offchainViewTester = TestOffchainView(c1.token_metadata)
        scenario.register(offchainViewTester)
        offchainViewTester.compute(data = c1.data, params = 0)
        scenario.verify_equal(
            offchainViewTester.data.result,
            sp.some(
                sp.record(
                    token_id = 0,
                    token_info = sp.map({
                        "decimals"    : sp.utils.bytes_of_string("2"),
                        "name"        : sp.utils.bytes_of_string("JimsterToken"),
                        "symbol"      : sp.utils.bytes_of_string("JIM"),
                        "icon"        : sp.utils.bytes_of_string("https://costchain.roelink.eu/static/icon.png")
                    })
                )
            )
        )

        scenario.h1("Entry points")
        scenario.h2("Set Alice's and Bob's balance to 0")
        c1.requestSetBalance(address = alice.address, newBalance = 0).run(sender = alice)
        c1.requestSetBalance(address = bob.address, newBalance = 0).run(sender = bob)
        c1.approveSetBalance(alice.address).run(sender = bob)
        c1.approveSetBalance(bob.address).run(sender = alice)
        c1.setBalance(alice.address).run(sender = alice)
        c1.setBalance(bob.address).run(sender = bob)

        scenario.h2("Admin mints a few coins")
        c1.mint(address = alice.address, value = 12).run(sender = alice)
        c1.mint(address = bob.address, value = 3).run(sender = alice)
        c1.mint(address = alice.address, value = 3).run(sender = bob)
        c1.burn(address = bob.address, value = 10).run(sender = alice)
        scenario.verify(c1.data.totalSupply == 16)
        scenario.verify(c1.data.balances[alice.address].balance == 8)
        scenario.verify(c1.data.balances[bob.address].balance == 8)

        scenario.h2("Alice wants to set her balance (not allowed)")
        c1.setBalance(alice.address).run(sender = alice, valid = False)

        scenario.h2("Alice sends a request to set her balance")
        c1.requestSetBalance(address = alice.address, newBalance = 42).run(sender = alice)

        scenario.h2("Alice wants to set her balance (not allowed)")
        c1.setBalance(alice.address).run(sender = alice, valid = False)

        scenario.h2("Bob approves")
        c1.approveSetBalance(alice.address).run(sender = bob)

        scenario.h2("Alice wants to set her balance (now allowed)")
        c1.setBalance(alice.address).run(sender = alice)

        scenario.h2("Alice wants to set her balance (not allowed again)")
        c1.setBalance(alice.address).run(sender = alice, valid = False)

        scenario.h2("Alice wants to burn 10 tokens (not possible because Bob has less than 10 tokens)")
        c1.burn(address = alice.address, value = 10).run(sender = alice, valid = False)

        scenario.h2("Alice wants to burn 4 tokens")
        c1.burn(address = alice.address, value = 4).run(sender = alice)

        scenario.h2("Alice wants to transfer 8 tokens to Bob with a message")
        c1.transferWithMessage(from_ = alice.address, to_ = bob.address, value = 8, message = 'Hi Bob').run(sender = alice)
        scenario.verify(c1.data.balances[alice.address].balance == 30)

        scenario.h1("Views")
        scenario.h2("Balance")
        view_balance = Viewer(sp.TNat)
        scenario += view_balance
        c1.getBalance((alice.address, view_balance.typed.target))
        scenario.verify_equal(view_balance.data.last, sp.some(30))

        scenario.h2("Administrators")
        view_administrators = Viewer(sp.TSet(sp.TAddress))
        scenario += view_administrators
        c1.getAdministrators((sp.unit, view_administrators.typed.target))
        scenario.verify_equal(view_administrators.data.last, sp.some(sp.set([alice.address, bob.address])))

        scenario.h2("Total Supply")
        view_totalSupply = Viewer(sp.TNat)
        scenario += view_totalSupply
        c1.getTotalSupply((sp.unit, view_totalSupply.typed.target))
        scenario.verify_equal(view_totalSupply.data.last, sp.some(42))

    sp.add_compilation_target(
        "FA1_2",
        FA12(
            admins  = sp.set([sp.address("tz1VjbsavUSvsCbbGJ8kJWvNSJ81WXiQFBcf"), sp.address("tz1fEz5UP2dHyaJR4cKRnQXjP3RUeMXfMrbC")]),
            config  = FA12_config(
                support_upgradable_metadata         = True,
                use_token_metadata_offchain_view    = True
            ),
            token_metadata = {
                "decimals"    : "2",             # Mandatory by the spec
                "name"        : "JimsterToken",  # Recommended
                "symbol"      : "JIM",           # Recommended
                # Extra fields
                "icon"        : "https://costchain.roelink.eu/static/icon.png"
            },
            contract_metadata = None
        )
    )
